from tkinter.messagebox import *
from tkinter import * 


def button_validation_callback():
    showinfo('Greeting', 'Hello %s' % value.get())


root = Tk()
root.title("Greetings")

value = StringVar()
value.set('Default value')

label = Label(root, text="Hello World")
label.grid(column=0, row=0)

input_r = Entry(root, textvariable=value)
input_r.grid(column=1, row=0)


button_validation = Button(root, text="Say it !", command=button_validation_callback)

button_validation.grid(row=1, column=1)

root.rowconfigure(0, weight=1)
root.columnconfigure(1, weight=1)

input_r.grid(column=1, row=0, sticky='nsew')

root.mainloop()
