def textcount(char):
    value = 0
    for i in range(len(char)):
        if i == 0 and char[i] != " ":
            value = value + 1
        if i > 0 and char[i] != " " and char[i-1] == " ":
            value = value + 1
    return value


def textcount_opti(string):
    if len(string):
        words = string.split(' ')
        return len(words)
    return 0
#Version du prof