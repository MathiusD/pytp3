import unittest
from strings.functions import textcount, textcount_opti

class Testvim(unittest.TestCase):
    
    def test_textcount(self):
        self.assertEqual(textcount("Hello World !"), 3)
        self.assertEqual(textcount("World !"), 2)
        self.assertEqual(textcount("Hello"), 1)
        self.assertEqual(textcount(""), 0)

    def test_textcount_opti(self):
        self.assertEqual(textcount_opti("Hello World !"), 3)
        self.assertEqual(textcount_opti("World !"), 2)
        self.assertEqual(textcount_opti("Hello"), 1)
        self.assertEqual(textcount_opti(""), 0)
