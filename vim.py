from tkinter.messagebox import *
from tkinter import * 
from strings.functions import textcount


def button_counter_callback():
    showinfo('Your Text', 'Your text is %i long.' % textcount(textarea.get("0.0", END)))

if __name__ == "__main__":

    root = Tk()
    root.title("Counter")

    textarea = Text(root, wrap='word') # création du composant
    textarea.insert(END, "Hello World") # insertion d'un texte
    textarea.grid(column=0, row=0)

    button_counter = Button(root, text="Count !", command=button_counter_callback)

    button_counter.grid(row=1, column=1)

    root.rowconfigure(0, weight=1)
    root.columnconfigure(1, weight=1)

    textarea.grid(column=1, row=0, sticky='nsew')

    root.mainloop()